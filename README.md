# README #

Kubernetes + Nexus + Jenkins

### Pre-requisitos ###

* Instalar Docker
* Instalar Minikube
* Instalar kubectl

### Before ###

* ejecutar minikube sin sudo : sudo nano ~/.bashrc -> agregar -> alias minikube = "sudo minikube"
* ejecutar kubectl sin sudo : sudo nano ~/.bashrc -> agregar -> alias kubectl = "sudo kubectl"
* minikube start --driver=none

### Run ###

* Ejecutar los comandos en steps.txt

### After ###

* abrir un terminal
* ejecutar sudo nano /etc/hosts
* agregar ipNodeMinikube gustavo.martinez.local
* agregar ipNodeMinikube gustavo.martinez.jenkins
* agregar ipNodeMinikube gustavo.martinez.nexus

### Verify ###

* Para acceder al dashboard : http://gustavo.martinez.local
* Para acceder al dashboard : http://gustavo.martinez.nexus
* Para acceder al dashboard : http://gustavo.martinez.jenkins

![Dashboard](https://bitbucket.org/gustavomartinezgranella/k8s-nexus-jenkins/raw/bfa86e8f322a25105a1b3b1ff87c817b09eb43e5/assets/dashboard.png)
![HealthStatus](https://bitbucket.org/gustavomartinezgranella/k8s-nexus-jenkins/raw/bfa86e8f322a25105a1b3b1ff87c817b09eb43e5/assets/health.png)
![Jenkins](https://bitbucket.org/gustavomartinezgranella/k8s-nexus-jenkins/raw/bfa86e8f322a25105a1b3b1ff87c817b09eb43e5/assets/jenkins.png)
![Nexus](https://bitbucket.org/gustavomartinezgranella/k8s-nexus-jenkins/raw/bfa86e8f322a25105a1b3b1ff87c817b09eb43e5/assets/nexus.png)